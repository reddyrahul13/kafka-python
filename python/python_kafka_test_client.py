from confluent_kafka.admin import AdminClient
from confluent_kafka import Consumer
from confluent_kafka import Producer
from sys import argv
from datetime import datetime
import os
import shutil
import glob
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

topic='NewTopic'

def Consume():
    print('\n<Consuming>')
    c = Consumer({
        'bootstrap.servers': bootstrap_server,
        'group.id': 'rmoff',
        'auto.offset.reset': 'earliest',
        'enable.auto.commit': True,
        'session.timeout.ms': 6000,
        'client.id': 'client-1',
        'default.topic.config': {'auto.offset.reset': 'smallest'}
    })

    c.subscribe(['topic'])
    i = 0
    try:
        while True:
            msg = c.poll(0.1)
            if msg is None:
                continue

            elif not msg.error():
                print(msg.value())
                source = msg.value()
                res = source.decode("utf-8")
                destination = "/var/www/html/new_vid"
                new_path = shutil.copy(res, destination)
                i+=1
                try:
                    connection = mysql.connector.connect(host='172.23.0.1', database='audio', user='root', password='root12')
                    mySql_insert_query = """INSERT IGNORE INTO Music (id, filepath) 
                                    VALUES (%s, %s) """
                    cursor = connection.cursor()
                    insert_tuple = (i, new_path)
                    cursor.execute(mySql_insert_query, insert_tuple)

                    connection.commit()
                    print("Record inserted successfully into Audios table")
                    cursor.close()
                except mysql.connector.Error as error:
                    print("Failed to insert record into Audio table {}".format(error))

            elif msg.error().code() == KafkaError._PARTITION_EOF:
                print('End of partition reached {0}/{1}'
                      .format(msg.topic(), msg.partition()))
            else:
                print('Error occured: {0}'.format(msg.error().str()))
                

    except KeyboardInterrupt:
        pass

    finally:
        c.close()

try:
    bs=argv[1]
    print('\n🥾 bootstrap server: {}'.format(bs))
    bootstrap_server=bs
except:
    # no bs X-D
    bootstrap_server='localhost:9092'
    print('⚠️  No bootstrap server defined, defaulting to {}\n'.format(bootstrap_server))

a = AdminClient({'bootstrap.servers': bootstrap_server})
try:         
    md=a.list_topics(timeout=10)
    print("""
    Connected to bootstrap server(%s) and it returned metadata for brokers as follows:

    %s
        ---------------------
        Bootstrap Connection Succesfuuly
    """
    % (bootstrap_server,md.brokers))
    
    
    try:
        # Produce()
        print('\n<Producer started>')
        p = Producer({'bootstrap.servers': bootstrap_server})

        def acked(err, msg):
            if err is not None:
                print("Failed to deliver message: {0}: {1}"
                      .format(msg.value(), err.str()))
            else:
                print("Message produced: {0}".format(msg.value()))

        # p = Producer(**conf)

        try:
            files = glob.glob("/var/www/html/new/*")
            for val in files:
                p.produce('topic', val, callback=acked)
                p.poll(0.5)

        except KeyboardInterrupt:
            pass

        p.flush(30)

        Consume()
        
    except:
        print("(uncaught exception in produce/consume)")


except Exception as e:
    print("""
    Failed to connect to bootstrap server.
    
    %s
    
    Check that Kafka is running, and that the bootstrap server you've provided (%s) is reachable from your client
    """
    % (e,bootstrap_server))
