from kafka import KafkaConsumer
import ujson
import requests
import time
import json
import os
import mysql.connector
from mysql.connector import Error
from datetime import datetime
import datetime

print("Waiting for other services to start...")
time.sleep(10)

headers = {
    'Accept': 'application/json',
	'Content-Type': 'application/json'
}

# check whether Kafka Connect is up
while True:
	try:
		time.sleep(5)
		response = requests.get('http://connect:8083/', headers=headers)
		print(response.text)
		break
	except requests.exceptions.ConnectionError:
		print('\nWaiting for Kafka Connect and retrying...')
		continue
print("Sending register request to Kafka Connect")
time.sleep(10)

print('\n')
headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

# read JSON file as string
debezium_conf = open('config.json').read()

# number of attempts to register Debezium connector
retries = 5 

# register Debezium connector with config
while True:
	time.sleep(5)
	try:
		response = requests.post('http://connect:8083/connectors/', headers=headers, data=debezium_conf)
		if response.status_code == 201 or response.status_code == 409:
			print(response.text)
			print('\nSuccessfully registered ')
			break
		else:
			print(response.text)
			print(response.status_code)
			print("Unsuccessful")
			retries = retries - 1
	except requests.exception as e:
		print(e)

print("Starting Consumer...")

# consume and deserialize JSON message to dict for easy parsing
# Include kwarg {auto_offset_reset='earliest'} to also read existing files in database
consumer = KafkaConsumer('cogknit-mysql-server.cogknit.file_loc',
			  value_deserializer=lambda m: ujson.loads(m.decode('utf-8')),
			  key_deserializer=lambda m: ujson.loads(m.decode('utf-8')),
			  bootstrap_servers=['kafka:9092'])


connection = mysql.connector.connect(
    host="mysql",
    database="cogknit",
    user="mysqluser",               #'root',
    password="mysqlpw"              #'debezium'
)
cursor = connection.cursor()

for message in consumer:
	# Handling messages
	print ("\n"+"="*10+" NEW EVENT "+"="*10+"\n")
	print ("\nDB: {}\nTable: {}\nFile Id: {}\nFilename: {}\nInsert Statement?: {}".format(
		message.value['payload']['source']['db'],
		message.value['payload']['source']['table'],
		message.key['payload']['id'],
		message.value['payload']['after']['filename'],
		#message.value['payload']['after']['FileName'],
		message.value['payload']['before'] == None
	))

	# Forward request for INSERT operation in specified db and table to data-processing-ms
	try:
		# Listen only to specified DB and Table in the environment 
		if (message.value['payload']['source']['table'] != os.environ['PROD_MYSQL_TABLE'] or message.value['payload']['source']['db'] != os.environ['PROD_MYSQL_DB_NAME']):
			raise Exception("Ignoring updates on table and/or db which is not specified")
		# Listen only for INSERT statements
		if (message.value['payload']['before'] is not None):
			raise Exception("Operation not an `INSERT` command")
		# Create payload with 3 items required for decoding
		# Model1_value  = "select 'Model_value' from intercept where FileName = 'filename' "
		
		file = message.value['payload']['after']['filename']
		m_v = f'SELECT Model_value FROM intercept WHERE  FileName = "{file}"'
		cursor.execute(m_v)
		model_val = cursor.fetchall()[0][0]
		m_d = f'SELECT Diarization FROM intercept WHERE  FileName = "{file}"'
		cursor.execute(m_d)
		diarization = cursor.fetchall()[0][0]
		#diarization = 2
		connection.commit()

		payload = {"conf":  {
			"file_id": message.value['payload']['after']['id'],
			"language": message.value['payload']['after']['lang'],
			#"filename": message.value['payload']['after']['FileName']
			"filename": file,
			"Model_value": model_val,
			"Diarization": diarization
		}, "replace_microseconds":"N"}


		print(payload)
		time.sleep(10)
		# trigger batch processing DAG with data
		response = requests.post('https://testblade.cogknit.com/airflow/api/experimental/dags/ASR-Decoding/dag_runs', headers=headers, json=payload)
#		response = requests.post('http://airflow:8080/api/experimental/dags/ASR-Decoding/dag_runs', headers=headers, json=payload)
		#response = requests.post('http://airflow:8080/api/experimental/dags/ASR-Decoding/dag_runs', headers=headers, json=payload)
		#response = requests.post('https://testblade.cogknit.com/airflow/api/experimental/dags/KALDI-kws/dag_runs', headers=headers, json=payload)
		print (response.json())
		#print (response.json())
		start = str(datetime.datetime.now())
		sql_nfe = f'UPDATE intercept SET ASR=1, KWS=1, status_code=1, ASR_start="{start}", KWS_start="{start}"  WHERE InterceptID="{file}"'
		cursor.execute(sql_nfe)
		sql_file = f'UPDATE file_loc SET asrvalue=1, KWS=1 WHERE filename="{file}"'
		cursor.execute(sql_file)
		connection.commit()
	except Exception as e:
		print ("Failed with error: {}".format)
	print ("\n"+"="*31+"\n")

