# In production you would almost certainly limit the replication user must be on the follower (slave) machine,
# to prevent other clients accessing the log from other machines. For example, 'replicator'@'follower.acme.com'.
#
# However, this grant is equivalent to specifying *any* hosts, which makes this easier since the docker host
# is not easily known to the Docker container. But don't do this in production.
#
CREATE USER 'replicator' IDENTIFIED BY 'replpass';
CREATE USER 'debezium' IDENTIFIED BY 'dbz';
ALTER USER 'debezium'@'%' IDENTIFIED WITH mysql_native_password BY 'dbz';
GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'replicator';
GRANT SELECT, RELOAD, SHOW DATABASES, REPLICATION SLAVE, REPLICATION CLIENT  ON *.* TO 'debezium';

# Create the database that we'll use to populate data and watch the effect in the binlog
CREATE DATABASE cogknit;
GRANT ALL PRIVILEGES ON cogknit.* TO 'mysqluser'@'%';

USE cogknit;

CREATE TABLE intercept (
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  InterceptID VARCHAR(75) NOT NULL,
  FileName VARCHAR(300) NOT NULL,
  FilePath VARCHAR(300) NOT NULL,
  ActualName VARCHAR(120) NOT NULL,
  NFE INTEGER DEFAULT 0,
  ASR INTEGER DEFAULT 0,
  MT INTEGER DEFAULT 0,
  TTS INTEGER DEFAULT 0,
  KWS INTEGER DEFAULT 0,
  SD INTEGER DEFAULT 0,
  status VARCHAR(20) NOT NULL DEFAULT 'Uploaded',
  user VARCHAR(120) NOT NULL DEFAULT '1',
  batchID VARCHAR(120) NOT NULL DEFAULT '000000',
  fileID VARCHAR(120) NOT NULL DEFAULT '000000',
  reason TEXT,
  hashValue VARCHAR(100) DEFAULT  NULL,
  oldFname VARCHAR(300) DEFAULT NULL,
  lang VARCHAR(255) NOT NULL DEFAULT 'Mandarin',
  MTValue TEXT,
  CreatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UpdatedOn TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  NFE_start TIMESTAMP NULL DEFAULT NULL,
  NFE_end TIMESTAMP NULL DEFAULT NULL,
  ASR_start TIMESTAMP NULL DEFAULT NULL,
  ASR_end TIMESTAMP NULL DEFAULT NULL,
  MT_start TIMESTAMP NULL DEFAULT NULL,
  MT_end TIMESTAMP NULL DEFAULT NULL,
  TTS_start TIMESTAMP NULL DEFAULT NULL,
  TTS_end TIMESTAMP NULL DEFAULT NULL,
  KWS_start TIMESTAMP NULL DEFAULT NULL,
  KWS_end TIMESTAMP NULL DEFAULT NULL,
  LID INTEGER DEFAULT 0,
  LID_start TIMESTAMP NULL DEFAULT NULL,
  LID_end TIMESTAMP NULL DEFAULT NULL,
  status_code INTEGER DEFAULT 1,
  LID_required BOOLEAN DEFAULT TRUE,
  PUN INTEGER DEFAULT 0,
  PUN_start TIMESTAMP NULL DEFAULT NULL,
  PUN_end TIMESTAMP NULL DEFAULT NULL,
  Model_value VARCHAR(300) DEFAULT '0',
  Diarization INTEGER DEFAULT 0,
  Execute_date VARCHAR(120) NOT NULL DEFAULT '1',
  Failure_status BOOLEAN DEFAULT TRUE
);

CREATE TABLE file_loc (
  id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  filename VARCHAR(255) NOT NULL,
  old_fname VARCHAR(255) NOT NULL,
  lang VARCHAR(255) NOT NULL,
  asrvalue INTEGER,
  mtValue INTEGER,
  ttsValue INTEGER,
  nfeValue INTEGER,
  KWS INTEGER,
  time_stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Model_value VARCHAR(300) DEFAULT '0'
);
