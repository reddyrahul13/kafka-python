# Decoding Service
This repository implements the data ingestion microservice architecture described [here](https://docs.google.com/document/d/19cu_kwAxy4etsJ62_SdgVETg-_gVksp6Wjb8PzVlBgc/edit?usp=sharing) for the Cogknit ESPnet workflow orchestration.

# Data Ingestion Microservice
This microservice is responsible for two separate process flow:  
 - `Single File Processing` (by Cogknit Team) Using Flask App to communicate with the decoding DAGs
 - `Batch File Processing` (by Eder Team) Using MySQL connector to listen and trigger batch processing on decoding DAGs  
 
## Batch Processing
 MySQL connector is implemented using Debezium MySQL connector, build on top of Kafka and Kafka Connect. Following are the services which are initialized and run in order to listen and report of changing events to the `Data Processing Microservice`.
  - `Zookeeper` (Kafka Dependency)  
  - `Kafka Broker`
  - `Kafka Connect`  
  Hosts MySQL connector which listens to change events and streams data to a defined kafka topic 
  - `Kafka Consumer`  
  Consumes updates on kafka topic to trigger Airflow DAG for batch processing  

For testing convenience and reference, a sample MySQL server image is integrated with the docker-compose.yml built from `./mysql` directory, for which the following configurations is used:
  - Server Name: cogknit-mysql-server
  - Database: cogknit
  - Table: file_loc
  - Table Schema: (as shared in [this](https://docs.google.com/document/d/1o6s6pU-Li-VFV_5bUT1fYDVIGi1foxFqiu5S_6mE3Cw/edit?ts=5f33b099) document)

Debezium MySQL Connector using Kafka Connect creates 2 separate topics for MySQL events:
  - `cogknit-mysql-server`  
  This topic produces MySQL schema change events that include all DDL statements applied to databases in the MySQL server.
  - `cogknit-mysql-server.cogknit.file_loc`  
  This topic updates events related to **file_loc** which includes INSERT, UPDATE, and DELETE operations.

### Requirements
Currently tested with Ubuntu 16.04 system with `Docker` and `Docker-Compose` installed. You would also need to create the docker network if it does not already exist.
```
sudo docker network create cogknit_decoding_network
```

### Run
1. Start Services
```
sudo docker-compose up -d
```  
Starts a daemon which spawns docker containers for each of the services. This command on success produces the prints the following:
```
Creating network "espnet-workflow_default" with the default driver
Creating zookeeper     ... done
Creating mysql         ... done
Creating kafka         ... done
Creating connect       ... done
Creating kafkaconsumer ... done
```

Because all services are started at the same time, run.sh tries to check if the kafka Connect service is up. Once connected, it registers the Debezium connector using the JSON config as:
```
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @config.json
```

Creates new Debezium MySQL connector with configurations from `config.json`. Upon success, following is printed to the log console:
``` 
HTTP/1.1 201 Created
Date: Mon, 07 Sep 2020 02:01:09 GMT
Location: http://localhost:8083/connectors/mysql-decoding-connector
Content-Type: application/json
Content-Length: 510
Server: Jetty(9.4.24.v20191120)
{
    "name":"mysql-decoding-connector",
    "config": {...},
    "tasks":[],
    "type":"source"
}
```

Post that, the docker container for Kafka Consumer is built and will start printing messages as they come. The message would be a complicated JSON followed by its parsed message. To check for all outputs, run the following command:

```
sudo docker-compose logs -f kafkaconsumer
```

The parsed output from the Kafka Consumer should look like the following:

```
========== NEW EVENT ==========
DB: cogknit
Table: file_loc
File Id: 101
Filename: 5_1025_20170621144836
Insert Statement?: True
{'execution_date': '2020-10-08T17:30:21.143257+00:00', 'message': 'Created <DagRun ASR-Decoding @ 2020-10-08 17:30:21.143257+00:00: manual__2020-10-08T17:30:21.143257+00:00, externally triggered: True>', 'run_id': 'manual__2020-10-08T17:30:21.143257+00:00'}
===============================
```
