Follow the steps below to dockerize Kafka

1) Build the image for python using the following command
		
		docker build -t python_kafka_test_client .
		
2) Run the docker containers using the command

		docker-compose up -d