from confluent_kafka import Producer
import glob
import os


conf = {'bootstrap.servers': 'localhost:9092', 
             'queue.buffering.max.messages': 1000000, 
             'queue.buffering.max.ms' : 500, 
             'batch.num.messages': 50, 
             'default.topic.config': {'acks': 'all'}}

def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: {0}: {1}"
              .format(msg.value(), err.str()))
    else:
        print("Message produced: {0}".format(msg.value()))

p = Producer(**conf)

try:
    files = glob.glob("/var/www/html/new/*")
    latest_file = max(files, key=os.path.getctime)
    # for val in latest_file:
    p.produce('NewTopic', 'myvalue #{0}'
                  .format(latest_file), callback=acked)
    p.poll(0.5)

except KeyboardInterrupt:
    pass

p.flush(30)